Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Lomiri Online Accounts
Upstream-Contact: UBports developers <developers@ubports.com>
Source: https://gitlab.com/ubports/development/core/lomiri-online-accounts

Files: click-hooks/CMakeLists.txt
 click-hooks/online-accounts-hooks/CMakeLists.txt
 click-hooks/online-accounts-hooks2/CMakeLists.txt
 doc/CMakeLists.txt
 doc/online-accounts-client/CMakeLists.txt
 online-accounts/CMakeLists.txt
 online-accounts/lib/CMakeLists.txt
 online-accounts/lib/qml/CMakeLists.txt
 online-accounts/lib/qt/CMakeLists.txt
 online-accounts-client/CMakeLists.txt
 online-accounts-client/lib/CMakeLists.txt
 online-accounts-client/lib/LomiriOnlineAccountsClient/CMakeLists.txt
 online-accounts-client/lib/module/CMakeLists.txt
 online-accounts-daemon/lib/CMakeLists.txt
 tests/online-accounts-daemon/functional_tests/daemon/CMakeLists.txt
 online-accounts-plugins/CMakeLists.txt
 online-accounts-plugins/lib/CMakeLists.txt
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/CMakeLists.txt
 online-accounts-plugins/lib/module/CMakeLists.txt
 online-accounts-service/CMakeLists.txt
 online-accounts-service/src/CMakeLists.txt
 online-accounts-ui/CMakeLists.txt
 online-accounts-ui/src/CMakeLists.txt
 tests/CMakeLists.txt
 tests/click-hooks/CMakeLists.txt
 tests/click-hooks/online-accounts-hooks/CMakeLists.txt
 tests/click-hooks/online-accounts-hooks2/CMakeLists.txt
 tests/online-accounts-client/CMakeLists.txt
 tests/online-accounts-client/lib/CMakeLists.txt
 tests/online-accounts-client/lib/tst_client/CMakeLists.txt
 tests/online-accounts-client/lib/tst_qml_client/CMakeLists.txt
 tests/online-accounts-plugins/CMakeLists.txt
 tests/online-accounts-plugins/lib/CMakeLists.txt
 tests/online-accounts-service/CMakeLists.txt
 tests/online-accounts-service/tst_inactivity_timer/CMakeLists.txt
 tests/online-accounts-service/tst_libaccounts_service/CMakeLists.txt
 tests/online-accounts-service/tst_service/CMakeLists.txt
 tests/online-accounts-service/tst_signonui_service/CMakeLists.txt
 tests/online-accounts-service/tst_ui_proxy/CMakeLists.txt
 tests/online-accounts-ui/CMakeLists.txt
 tests/online-accounts-ui/tst_access_model/CMakeLists.txt
 tests/online-accounts-ui/tst_browser_request/CMakeLists.txt
 tests/online-accounts-ui/tst_notification/CMakeLists.txt
 tests/online-accounts-ui/tst_provider_request/CMakeLists.txt
 tests/online-accounts-ui/tst_qmlplugin/CMakeLists.txt
 tests/online-accounts-ui/tst_signonui_request/CMakeLists.txt
Copyright: 2024, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3
Comment:
 The CMake files have been reworked by Mike Gabriel (known from Git history).

Files: AUTHORS
 CMakeLists.txt
 ChangeLog
 click-hooks/online-accounts-hooks/account-application.hook.in
 click-hooks/online-accounts-hooks/account-provider.hook.in
 click-hooks/online-accounts-hooks/account-qml-plugin.hook
 click-hooks/online-accounts-hooks/account-service.hook.in
 click-hooks/online-accounts-hooks2/accounts.hook.in
 cmake/ParseArguments.cmake
 doc/online-accounts-client/css/qtquick.css
 doc/online-accounts-client/css/scratch.css
 doc/online-accounts-client/moduledef.qdoc
 doc/online-accounts/CMakeLists.txt
 doc/online-accounts/css/custom.css
 doc/online-accounts/lomiri-onlineaccounts2.qdocconf
 doc/online-accounts/moduledef.qdoc
 examples/client/CreateFacebook.qml
 examples/plugins/example.pro
 examples/plugins/example.provider
 online-accounts-client/lib/LomiriOnlineAccountsClient/LomiriOnlineAccountsClient.pc.in
 online-accounts-client/lib/LomiriOnlineAccountsClient/Setup
 online-accounts-client/lib/module/qmldir.in
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/LomiriOnlineAccountsPlugin.pc.in
 online-accounts-plugins/lib/module/ChromedWebView.qml
 online-accounts-plugins/lib/module/WebView.qml
 online-accounts-plugins/lib/module/qmldir.in
 online-accounts-service/src/com.canonical.indicators.webcredentials.xml
 online-accounts-service/src/com.lomiri.OnlineAccounts.Manager.service.in
 online-accounts-service/src/com.lomiri.OnlineAccountsUi.service.in
 online-accounts-service/src/com.lomiri.OnlineAccountsUi.xml
 online-accounts-ui/src/lomiri-online-accounts-ui.desktop.in
 online-accounts-ui/src/qml/SignOnUiDialog.qml
 online-accounts-ui/src/qml/SignOnUiPage.qml
 online-accounts-ui/src/ui.qrc
 po/CMakeLists.txt
 po/LINGUAS
 po/POTFILES.in
 po/*.po
 po/lomiri-online-accounts.pot
 tests/online-accounts-service/tst_ui_proxy/data/com.lomiri.test_confined.provider
 tests/online-accounts-ui/data/applications/mailer.desktop
 tests/online-accounts-ui/data/com.ubuntu.tests_application.application
 tests/online-accounts-ui/data/cool.provider
 tests/online-accounts-ui/data/coolmail.service
 tests/online-accounts-ui/data/coolshare.service
 tests/online-accounts-ui/data/mailer.application
 tests/online-accounts-ui/tst_provider_request/tst_provider_request.qrc
 tests/online-accounts-ui/tst_qmlplugin/Source/qmldir
 tests/online-accounts-ui/tst_qmlplugin/testPlugin/Main.qml
 tests/online-accounts-ui/tst_qmlplugin/tst_AccountCreationPage.qml
 tests/online-accounts-ui/tst_qmlplugin/tst_AuthorizationPage.qml
 tests/online-accounts-ui/tst_qmlplugin/tst_SignOnUiDialog.qml
 tests/online-accounts-ui/tst_qmlplugin/tst_online_accounts_qml.cpp
Copyright: 2011-2016, Canonical Ltd.
License: GPL-3
Comment:
 Assuming license and copyright holdership from files in online-accounts-daemon/.

Files:
 online-accounts/lib/qml/Lomiri/OnlineAccounts/CMakeLists.txt
 online-accounts/lib/qml/Lomiri/OnlineAccounts/qmldir.in
 online-accounts/lib/qml/Ubuntu/OnlineAccounts/CMakeLists.txt
 online-accounts/lib/qml/Ubuntu/OnlineAccounts/qmldir
 online-accounts/lib/qt/LomiriOnlineAccounts/Account
 online-accounts/lib/qt/LomiriOnlineAccounts/AuthenticationData
 online-accounts/lib/qt/LomiriOnlineAccounts/AuthenticationReply
 online-accounts/lib/qt/LomiriOnlineAccounts/CMakeLists.txt
 online-accounts/lib/qt/LomiriOnlineAccounts/Error
 online-accounts/lib/qt/LomiriOnlineAccounts/LomiriOnlineAccountsQt.pc.in
 online-accounts/lib/qt/LomiriOnlineAccounts/Manager
 online-accounts/lib/qt/LomiriOnlineAccounts/OAuth1Data
 online-accounts/lib/qt/LomiriOnlineAccounts/OAuth2Data
 online-accounts/lib/qt/LomiriOnlineAccounts/PasswordData
 online-accounts/lib/qt/LomiriOnlineAccounts/PendingCall
 online-accounts/lib/qt/LomiriOnlineAccounts/PendingCallWatcher
 online-accounts/lib/qt/LomiriOnlineAccounts/SaslData
 online-accounts/lib/qt/LomiriOnlineAccounts/Service
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/CMakeLists.txt
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/LomiriOnlineAccountsDaemon.pc.in
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/Manager
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/com.lomiri.OnlineAccounts.Manager.xml
 online-accounts-daemon/CMakeLists.txt
 tests/online-accounts/CMakeLists.txt
 tests/online-accounts/OnlineAccounts/CMakeLists.txt
 tests/online-accounts/OnlineAccounts/functional_tests/CMakeLists.txt
 tests/online-accounts/OnlineAccounts/tst_authentication_data/CMakeLists.txt
 tests/online-accounts/qml_module/CMakeLists.txt
 tests/online-accounts-daemon/CMakeLists.txt
 tests/online-accounts-daemon/functional_tests/CMakeLists.txt
 tests/online-accounts-daemon/functional_tests/data/applications/mailer.desktop
 tests/online-accounts-daemon/functional_tests/data/com.lomiri.OnlineAccounts.Manager.service.in
 tests/online-accounts-daemon/functional_tests/data/com.lomiri.tests_application.application
 tests/online-accounts-daemon/functional_tests/data/com.lomiri.tests_coolshare.service
 tests/online-accounts-daemon/functional_tests/data/cool.provider
 tests/online-accounts-daemon/functional_tests/data/coolmail.service
 tests/online-accounts-daemon/functional_tests/data/mailer.application
 tests/online-accounts-daemon/functional_tests/data/missing-provider.service
 tests/online-accounts-daemon/functional_tests/data/oauth1auth.service
 tests/online-accounts-daemon/functional_tests/data/testsession.conf.in
 tests/online-accounts-daemon/functional_tests/loa-accountd-wrapper.in
 tests/online-accounts-daemon/functional_tests/test_process.py
Copyright: 2011-2016, Canonical Ltd.
  2024, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: LGPL-3
Comment:
 Assuming license and copyright holdership from files in online-accounts-daemon/.
 .
 The CMake files have been reworked by Mike Gabriel (known from Git history).

Files: click-hooks/online-accounts-hooks/main.cpp
 click-hooks/online-accounts-hooks2/accounts.cpp
 click-hooks/online-accounts-hooks2/acl-updater.cpp
 click-hooks/online-accounts-hooks2/acl-updater.h
 examples/plugins/Main.qml
 online-accounts-common/debug.cpp
 online-accounts-common/debug.h
 online-accounts-common/globals.h
 online-accounts-common/i18n.cpp
 online-accounts-common/i18n.h
 online-accounts-common/ipc.cpp
 online-accounts-common/ipc.h
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/account-manager.cpp
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/account-manager.h
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/application-manager.cpp
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/application-manager.h
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/request-handler.cpp
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/request-handler.h
 online-accounts-plugins/lib/module/DuplicateAccount.qml
 online-accounts-plugins/lib/module/ErrorItem.qml
 online-accounts-plugins/lib/module/KeyboardRectangle.qml
 online-accounts-plugins/lib/module/OAuth.qml
 online-accounts-plugins/lib/module/OAuthMain.qml
 online-accounts-plugins/lib/module/Options.qml
 online-accounts-plugins/lib/module/RemovalConfirmation.qml
 online-accounts-plugins/lib/module/ServiceItem.qml
 online-accounts-plugins/lib/module/ServiceItemBase.qml
 online-accounts-plugins/lib/module/ServiceSwitches.qml
 online-accounts-plugins/lib/module/StandardAnimation.qml
 online-accounts-service/src/inactivity-timer.cpp
 online-accounts-service/src/inactivity-timer.h
 online-accounts-service/src/indicator-service.cpp
 online-accounts-service/src/indicator-service.h
 online-accounts-service/src/libaccounts-service.cpp
 online-accounts-service/src/libaccounts-service.h
 online-accounts-service/src/main.cpp
 online-accounts-service/src/mir-helper-stub.cpp
 online-accounts-service/src/mir-helper.cpp
 online-accounts-service/src/mir-helper.h
 online-accounts-service/src/notification.cpp
 online-accounts-service/src/notification.h
 online-accounts-service/src/reauthenticator.cpp
 online-accounts-service/src/reauthenticator.h
 online-accounts-service/src/request-manager.cpp
 online-accounts-service/src/request-manager.h
 online-accounts-service/src/request.cpp
 online-accounts-service/src/request.h
 online-accounts-service/src/service.cpp
 online-accounts-service/src/service.h
 online-accounts-service/src/signonui-service.cpp
 online-accounts-service/src/signonui-service.h
 online-accounts-service/src/ui-proxy.cpp
 online-accounts-service/src/ui-proxy.h
 online-accounts-service/src/utils.cpp
 online-accounts-service/src/utils.h
 online-accounts-ui/src/access-model.cpp
 online-accounts-ui/src/access-model.h
 online-accounts-ui/src/browser-request.cpp
 online-accounts-ui/src/browser-request.h
 online-accounts-ui/src/dialog-request.cpp
 online-accounts-ui/src/dialog-request.h
 online-accounts-ui/src/dialog.cpp
 online-accounts-ui/src/dialog.h
 online-accounts-ui/src/main.cpp
 online-accounts-ui/src/provider-request.cpp
 online-accounts-ui/src/provider-request.h
 online-accounts-ui/src/qml/AccountCreationPage.qml
 online-accounts-ui/src/qml/AuthorizationPage.qml
 online-accounts-ui/src/qml/ProviderRequest.qml
 online-accounts-ui/src/request.cpp
 online-accounts-ui/src/request.h
 online-accounts-ui/src/signonui-request.cpp
 online-accounts-ui/src/signonui-request.h
 online-accounts-ui/src/ui-server.cpp
 online-accounts-ui/src/ui-server.h
 tests/click-hooks/online-accounts-hooks/tst_online_accounts_hooks.cpp
 tests/click-hooks/online-accounts-hooks2/tst_online_accounts_hooks2.cpp
 tests/online-accounts-service/tst_inactivity_timer/tst_inactivity_timer.cpp
 tests/online-accounts-service/tst_libaccounts_service/tst_libaccounts_service.cpp
 tests/online-accounts-service/tst_service/tst_service.cpp
 tests/online-accounts-service/tst_signonui_service/request-manager-mock.cpp
 tests/online-accounts-service/tst_signonui_service/request-manager-mock.h
 tests/online-accounts-service/tst_signonui_service/tst_signonui_service.cpp
 tests/online-accounts-service/tst_ui_proxy/request-mock.cpp
 tests/online-accounts-service/tst_ui_proxy/request-mock.h
 tests/online-accounts-service/tst_ui_proxy/tst_ui_proxy.cpp
 tests/online-accounts-ui/mock/application-manager-mock.cpp
 tests/online-accounts-ui/mock/application-manager-mock.h
 tests/online-accounts-ui/mock/notification-mock.cpp
 tests/online-accounts-ui/mock/notification-mock.h
 tests/online-accounts-ui/mock/qwindow.cpp
 tests/online-accounts-ui/mock/request-mock.cpp
 tests/online-accounts-ui/mock/request-mock.h
 tests/online-accounts-ui/mock/signonui-request-mock.cpp
 tests/online-accounts-ui/mock/signonui-request-mock.h
 tests/online-accounts-ui/mock/ui-server-mock.cpp
 tests/online-accounts-ui/mock/ui-server-mock.h
 tests/online-accounts-ui/tst_access_model/tst_access_model.cpp
 tests/online-accounts-ui/tst_browser_request/tst_browser_request.cpp
 tests/online-accounts-ui/tst_notification/tst_notification.cpp
 tests/online-accounts-ui/tst_provider_request/ProviderRequest.qml
 tests/online-accounts-ui/tst_provider_request/tst_provider_request.cpp
 tests/online-accounts-ui/tst_signonui_request/tst_signonui_request.cpp
 tests/online-accounts-ui/tst_signonui_request/window-watcher.h
 tests/online-accounts-plugins/lib/tst_application_manager.cpp
Copyright: 2011, Canonical Ltd.
  2011-2014, Canonical Ltd.
  2012, Canonical Ltd.
  2013, Canonical Ltd.
  2013-2015, Canonical Ltd.
  2013-2016, Canonical Ltd.
  2014, Canonical Ltd.
  2015, Canonical Ltd.
  2016, Canonical Ltd.
License: GPL-3

Files: doc/online-accounts/css/breadcrumbs.js
 doc/online-accounts/css/qtquick.css
 doc/online-accounts/css/scratch.css
 online-accounts/lib/qml/Lomiri/OnlineAccounts/account.cpp
 online-accounts/lib/qml/Lomiri/OnlineAccounts/account.h
 online-accounts/lib/qml/Lomiri/OnlineAccounts/account_model.cpp
 online-accounts/lib/qml/Lomiri/OnlineAccounts/account_model.h
 online-accounts/lib/qml/Lomiri/OnlineAccounts/authentication_data.cpp
 online-accounts/lib/qml/Lomiri/OnlineAccounts/authentication_data.h
 online-accounts/lib/qml/Lomiri/OnlineAccounts/plugin.cpp
 online-accounts/lib/qml/Lomiri/OnlineAccounts/plugin.h
 online-accounts/lib/qt/LomiriOnlineAccounts/account.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/account.h
 online-accounts/lib/qt/LomiriOnlineAccounts/account_info.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/account_info.h
 online-accounts/lib/qt/LomiriOnlineAccounts/account_p.h
 online-accounts/lib/qt/LomiriOnlineAccounts/authentication_data.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/authentication_data.h
 online-accounts/lib/qt/LomiriOnlineAccounts/authentication_data_p.h
 online-accounts/lib/qt/LomiriOnlineAccounts/authentication_reply.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/dbus_interface.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/dbus_interface.h
 online-accounts/lib/qt/LomiriOnlineAccounts/error.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/error.h
 online-accounts/lib/qt/LomiriOnlineAccounts/error_p.h
 online-accounts/lib/qt/LomiriOnlineAccounts/global.h
 online-accounts/lib/qt/LomiriOnlineAccounts/manager.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/manager.h
 online-accounts/lib/qt/LomiriOnlineAccounts/manager_p.h
 online-accounts/lib/qt/LomiriOnlineAccounts/pending_call.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/pending_call.h
 online-accounts/lib/qt/LomiriOnlineAccounts/pending_call_p.h
 online-accounts/lib/qt/LomiriOnlineAccounts/request_access_reply.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/service.cpp
 online-accounts/lib/qt/LomiriOnlineAccounts/service.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/access_request.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/access_request.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/account_info.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/async_operation.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/async_operation.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/authentication_request.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/authentication_request.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/authenticator.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/authenticator.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/client_registry.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/client_registry.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/dbus_constants.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/global.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/i18n.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/i18n.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/manager.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/manager.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/manager_adaptor.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/manager_adaptor.h
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/state_saver.cpp
 online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/state_saver.h
 tests/click-hooks/online-accounts-hooks2/fake_signond.h
 tests/online-accounts-daemon/functional_tests/daemon/inactivity_timer.cpp
 tests/online-accounts-daemon/functional_tests/daemon/inactivity_timer.h
 tests/online-accounts-daemon/functional_tests/daemon/main.cpp
 tests/online-accounts-daemon/functional_tests/daemon_interface.cpp
 tests/online-accounts-daemon/functional_tests/daemon_interface.h
 tests/online-accounts-daemon/functional_tests/fake_dbus_apparmor.h
 tests/online-accounts-daemon/functional_tests/fake_online_accounts_service.h
 tests/online-accounts-daemon/functional_tests/fake_signond.h
 tests/online-accounts-daemon/functional_tests/functional_tests.cpp
 tests/online-accounts/OnlineAccounts/functional_tests/functional_tests.cpp
 tests/online-accounts/OnlineAccounts/tst_authentication_data/tst_authentication_data.cpp
 tests/online-accounts/qml_module/tst_qml_module.cpp
Copyright: 2013, Canonical Ltd.
  2015, Canonical Ltd.
  2015-2016, Canonical Ltd.
  2016, Canonical Ltd.
License: LGPL-3

Files: po/ar.po
 po/ca.po
 po/cs.po
 po/de.po
 po/en_AU.po
 po/es.po
 po/fi.po
 po/fr.po
 po/fr_CA.po
 po/gl.po
 po/he.po
 po/hu.po
 po/it.po
 po/km.po
 po/ko.po
 po/lo.po
 po/lv.po
 po/my.po
 po/nl.po
 po/pl.po
 po/pt_BR.po
 po/ru.po
 po/shn.po
 po/sl.po
 po/ta.po
 po/tr.po
 po/ug.po
 po/uk.po
 po/zh_CN.po
 po/zh_TW.po
 po/am.po
 po/ast.po
 po/az.po
 po/br.po
 po/bs.po
 po/ca@valencia.po
 po/ckb.po
 po/da.po
 po/el.po
 po/en_GB.po
 po/eu.po
 po/fa.po
 po/gd.po
 po/id.po
 po/is.po
 po/ja.po
 po/ms.po
 po/nb.po
 po/pa.po
 po/pt.po
 po/ro.po
 po/sr.po
 po/sv.po
 po/te.po
 po/aa.po
 po/be.po
 po/cy.po
 po/eo.po
 po/hi.po
 po/hr.po
 po/ia.po
 po/mr.po
 po/ne.po
 po/oc.po
 po/sq.po
 po/th.po
 po/vi.po
 po/sk.po
 po/zh_HK.po
Copyright: 2013, Rosetta Contributors and Canonical Ltd.
  2014, Rosetta Contributors and Canonical Ltd.
  2015, Rosetta Contributors and Canonical Ltd.
  2016, Rosetta Contributors and Canonical Ltd 2016
License: GPL-3
Comment:
 Assuming license from client part of lomiri-online-accounts.

Files: online-accounts-client/lib/LomiriOnlineAccountsClient/global.h
 online-accounts-client/lib/LomiriOnlineAccountsClient/setup.cpp
 online-accounts-client/lib/LomiriOnlineAccountsClient/setup.h
 online-accounts-client/lib/module/plugin.cpp
 online-accounts-client/lib/module/plugin.h
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/global.h
 online-accounts-plugins/lib/module/plugin.cpp
 online-accounts-plugins/lib/module/plugin.h
 tests/online-accounts-client/lib/tst_client/tst_client.cpp
 tests/online-accounts-client/lib/tst_qml_client/tst_qml_client.cpp
Copyright: 2013, Canonical Ltd.
  2013-2015, Canonical Ltd.
  2014, Canonical Ltd.
License: LGPL-3+

Files: online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/loopback-server.cpp
 online-accounts-plugins/lib/LomiriOnlineAccountsPlugin/loopback-server.h
 online-accounts-plugins/lib/module/qml-loopback-server.cpp
 online-accounts-plugins/lib/module/qml-loopback-server.h
Copyright: 2017-2020, Alberto Mardegan <mardy@users.sourceforge.net>
License: LGPL-2.1

Files: cmake/EnableCoverageReport.cmake
 cmake/FindLcov.cmake
 cmake/Findgcovr.cmake
Copyright: 2010, Johannes Wienke <jwienke at techfak dot uni-bielefeld dot de>
  2011, Johannes Wienke <jwienke at techfak dot uni-bielefeld dot de>
License: GPL-2+

Files: tests/click-hooks/online-accounts-hooks2/signond.py
 tests/online-accounts-daemon/functional_tests/dbus_apparmor.py
 tests/online-accounts-daemon/functional_tests/online_accounts-service.py
 tests/online-accounts-daemon/functional_tests/signond.py
Copyright: NONE
License: LGPL-3+

Files: online-accounts-ui/src/external-browser-request.cpp
 online-accounts-ui/src/external-browser-request.h
Copyright: 2021, UBports Foundation
License: GPL-3

Files: doc/online-accounts-client/css/base.css
 doc/online-accounts-client/online-accounts-client.qdocconf
Copyright: 2011, Canonical Ltd.
  2013, Canonical Ltd.
License: GPL-3
Comment:
 Assuming license from online-accounts-client/ code.

Files: doc/online-accounts/css/base.css
Copyright: 2011, Canonical Ltd.
License: LGPL-3
Comment:
 Assuming license from online-accounts-daemon/ code.

Files: doc/online-accounts-client/css/reset.css
 doc/online-accounts/css/reset.css
Copyright: 2010, Yahoo! Inc.
License: BSD-3-clause

Files: debian/*
Copyright: 2024, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+ or LGPL-2.1 or LGPL-3+ or BSD-3-clause

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-3'

License: LGPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of version 3 of the GNU Lesser General Public
 License as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1
 This program is free software: you can redistribute it and/or modify
 it under the terms of version 3 of the GNU Lesser General Public
 License as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3+
 This package is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in /usr/share/common-licenses/LGPL-3.

License: BSD-3-clause
 Redistribution and use of this software in source and binary forms,
 with or without modification, are permitted provided that the following
 conditions are met:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 Neither the name of Yahoo! Inc. nor the names of YUI's contributors may
 be used to endorse or promote products derived from this software
 without specific prior written permission of Yahoo! Inc.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
