2025-02-28 Mike Gabriel

        * Release 0.16 (HEAD -> main, tag: 0.16)

2025-02-27 Mike Gabriel

        * Merge branch 'personal/lduboeuf/fix_header' into 'main' (a424098)

2025-02-26 Lionel Duboeuf

        * UIs: Fix content anchoring. (485dadf)

2025-02-27 Mike Gabriel

        * Merge branch 'personal/lduboeuf/renamings' into 'main' (0a93bb5)

2025-02-21 Lionel Duboeuf

        * system-settings->lomiri-system-settings renamings (303f198)

2025-02-18 Mike Gabriel

        * Merge branch 'personal/lduboeuf/deprecated_fix' into 'main'
          (7f9dc73)

2025-02-18 Lionel Duboeuf

        * Change Connections handler name according to new syntax (bde0447)
        * Move deprecated Page.title to PageHeader.title This fixes warning:
          In Lomiri.Components 1.3, the use of Page.title,
          Page.flickable and Page.head is deprecated. Use
          Page.header and the PageHeader component instead.
          (54835f6)

2025-02-01 Milo Ivir

        * Translated using Weblate (Croatian) (5cd32a2)

2025-01-12 தமிழ்நேரம்

        * Translated using Weblate (Tamil) (19487be)

2024-12-27 Hosted Weblate

        * Update translation files (313befc)

2024-12-23 Marius Gripsgard

        * Merge branch 'personal/peat-psuwit/fixup-compat-round-2' into
          'main' (7f95846)

2024-12-24 Ratchanan Srirattanamet

        * debian/: fix install path of Ubuntu.OnlineAccounts compat (ab444fd)
        * online-accounts: fix install path of Ubuntu.OnlineAccounts compat
          (2838a96)

2024-12-23 Mike Gabriel

        * Merge branch 'personal/peat-psuwit/fixup-compat' into 'main'
          (1d8cade)

2024-12-23 Ratchanan Srirattanamet

        * d/control: B:/R: on old qml-module-ubuntu-onlineaccounts (ce24277)
        * debian/: add back qml-module-ubuntu-onlineaccounts-client bin:pkg
          (1be7544)
        * online-accounts-client: restore Ubuntu.OnlineAccounts.Client compat
          (354a971)
        * online-accounts: fix QML plugin name for Ubuntu.OnlineAccounts
          compat (9cc1c1e)

2024-12-22 Ratchanan Srirattanamet

        * debian/: rename qml-module-ubuntu-onlineaccounts{ => 2} (4277e2f)

2024-10-12 ikozyris

        * Translated using Weblate (Greek) (533e983)

2024-09-10 Mike Gabriel

        * Merge branch 'fix/plugin-bindtextdomain' into 'main' (f32a7c8)

2024-09-08 OPNA2608

        * OnlineAccountsPlugin: Call bindtextdomain with buildtime-determined
          locale path (8742037)

2024-09-03 Ratchanan Srirattanamet

        * Merge branch 'personal/sunweaver/example-provider-path' into 'main'
          (ffe2d07)

2024-09-02 Mike Gabriel

        * examples/plugins/example.pro: Amend provider.path for new LOA
          namespacing. (6da76f0)

2024-08-31 Mike Gabriel

        * Release 0.15 (bb7aef7) (tag: 0.15)

2024-08-30 Mike Gabriel

        * Merge branch 'fix/gnuInstallDirs' into 'main' (8f59065)

2024-08-30 OPNA2608

        * online-accounts-ui/src/CMakeLists.txt: Use real variables for
          plugin dir defines (9857869)

2024-08-29 OPNA2608

        * online-accounts-service/src: Use CMAKE_INSTALL_FULL_*DIR variables
          where appropriate (3122d0a)
        * CMakeLists.txt: Determine QT_INSTALL_QML once at the top-level,
          instead of multiple times differently (3e14394)
        *.pc: Don't assume that CMAKE_INSTALL_*DIR are relative to
          CMAKE_INSTALL_PREFIX (1be2c42)

2024-08-30 Mike Gabriel

        * Merge branch 'fix/testingSetup' into 'main' (d7da986)

2024-08-29 OPNA2608

        * tst_access_model: Don't drop user-configured QML2_IMPORT_PATH
          (d2d1f0e)
        * tst_qmlplugin: Use dbus-test-runner to execute (7574151)
        * CMakeLists.txt: Use CTEST_TEST_TARGET_ALIAS method for check target
          (ea28609)
        * CMakeLists.txt: Import CTest for BUILD_TESTING initialisation
          (2ee43c3)

2024-08-25 Software In Interlingua

        * Translated using Weblate (Interlingua) (bedce81)

2024-08-13 Ricky From Hong Kong

        * Translated using Weblate (Chinese (Traditional)) (da481a4)

2024-08-11 umesaburo sagawa

        * Translated using Weblate (Japanese) (4e0d738)

2024-07-22 Jiri Grönroos

        * Translated using Weblate (Finnish) (f1b020a)

2024-07-18 Hosted Weblate

        * Update translation files (d86fdac)

2024-07-17 Languages add-on

        * Added translation using Weblate (Kurdish) (a14a250)

2024-07-17 Heimen Stoffels

        * Translated using Weblate (Dutch) (86650b4)

2024-07-17 Hosted Weblate

        * Update translation files (a72f2f5)

2024-07-17 Mike Gabriel

        * po/ku.po: Drop translation, using ckb/kmr/sdh instead. (9eaf7c3)

2024-07-17 Hosted Weblate

        * Update translation files (fd1e6b7)

2024-07-16 Mike Gabriel

        * Release 0.14 (a97063b) (tag: 0.14)

2024-07-16 Ratchanan Srirattanamet

        * Merge branch 'personal/sunweaver/tst-qmlplugin-fixes' into 'main'
          (2767e49)

2024-07-15 Mike Gabriel

        * d/control: Make B-D qml-module-sso-onlineaccounts as being required
          for unit tests, only. (8147b48)
        * d/control: Add B-D qml-module-lomiri-test (for unit tests).
          (0e3617e)
        * Lomiri.OnlineAccounts 2.0 vs. SSO.OnlineAccounts 0.1: Identify+fix
          more spots where the wrong OnlineAccounts implementation
          has been referenced / tried to use. (b4ad7d2)
        * tests/online-accounts-ui/tst_qmlplugin: Rework test case so that it
          smoothly builds and runs in CMake. (a4b5a37)
        * online-accounts-ui/src/qml/AccountCreationPage.qml: Be more robust
          if {local,system}QmlPluginPath don't end with a slash.
          (148072c)
        * online-accounts-ui/src/qml: Load correct OnlineAccounts
          implementation. (1b311f2)

2024-07-16 Ratchanan Srirattanamet

        * Merge branch 'personal/sunweaver/mir1client' into 'main' (c409d2a)

2024-07-13 Mike Gabriel

        * d/rules: Build with ENABLE_MIRCLIENT=ON for Ubuntu Touch. (1547e5e)
        * d/control: Add to B-D: libmir1client-dev (if available). (9128a5f)
        * online-accounts-service/src/CMakeLists.txt: Use pkg_search_module
          for finding mir1client or mirclient. (a747e1a)

2024-07-13 Ratchanan Srirattanamet

        * Merge branch 'personal/sunweaver-loa-click-hooks' into 'main'
          (86f0b6c)

2024-07-11 Mike Gabriel

        * Bump upstream version to 0.14. (460e39d)
        * d/control: l-o-a-client breaks l-s-s-online-accounts before 0.12.
          (f219b9c)
        * d/copyright: Update copyright attributions. (1576663)
        * d/control: Add to B-D: xmldiff (for unit tests). (ffc66b5)
        * d/control: Add to B-D: libclick-dev. (3da2c4e)
        * d/lomiri-online-accounts-client.install: Add click-hooks related
          files. (4e4a5fa)
        * File attributions: Be more precise to what component a code file
          belongs to (one-liner in the comment header). (24fe1b5)
        * Port click-hooks over from l-s-s-online-accounts. (93d07c7)
        * Revert "Port click-hooks over from l-s-s-online-accounts."
          (cc25bf4)
        * Revert "d/lomiri-online-accounts-client.install: Add click-hooks
          related files." (d80b3da)
        * Revert "Bump upstream version to 0.14." (ad19492)
        * Bump upstream version to 0.14. (ff0cc43)
        * d/lomiri-online-accounts-client.install: Add click-hooks related
          files. (453953e)
        * Port click-hooks over from l-s-s-online-accounts. (5f269ce)

2024-06-24 Mike Gabriel

        * Release 0.13 (97514be) (tag: 0.13)
        * debian/control: Assure same version of
          liblomiri-online-accounts-daemon is installed with
          lomiri-online-accounts-client bin:pkg. (0b9778f)
        * debian/control: Add to D (all dev:pkgs): Requirements as requested
          via .pc files. (933fbb3)
        * debian/liblomiri-online-accounts-plugin-dev.examples: White-space
          fix at EOF. (e311b3c)
        * d/copyright: Update file header. (3c892a8)
        * CMakeLists.txt: Set ONLINE_ACCCOUNTS_PLUGIN_DIR to
          Lomiri-namespaced path. (29325e4)

2024-06-23 Mike Gabriel

        * Release 0.12 (3d08691) (tag: 0.12)
        * debian/rules: Remove build $HOME during clean target. (6b4b902)

2024-06-19 Mike Gabriel

        * online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/: Drop
          exported oad_create_manager() C symbol and export
          OnlineAccountsDaemon::Manager class directly. (89de5de)
        *
          online-accounts-daemon/lib/LomiriOnlineAccountsDaemon/CMakeLists.txt:
          Install Manager / manager.h into include directory.
          (2977536)
        * online-accounts-service/: Stop loading
          OnlineAccountsDaemon::Manager dynamically. (390deff)

2024-06-10 Mike Gabriel

        * online-accounts/lib/qml/: Find better variable names for the
          LOA/UOA QML modules. (ed605b9)

2024-06-06 Mike Gabriel

        * debian/copyright: Update copyright attributions after refactoring
          of LOA. (4b3b7f6)

2024-06-10 Mike Gabriel

        * Prepare for next major Qt version, allow parameterizing QT_VERSION
          via CMake variable. (f1f7113)
        * tests/online-accounts-ui/tst_qmlplugin: Drop unused
          target_wrapper.sh script. (0c78d6c)
        * Move oa-service and oa-ui to the top level project directory.
          (2fcda09)
        * doc/: Adjust component namings once more (client ->
          online-accounts-client, daemon -> online-accounts).
          (49130b2)

2024-06-06 Mike Gabriel

        * d/rules: If derived from Ubuntu, disable the
          tst_libaccounts_service unit test. (cb8d119)
        * tests/online-accounts-client/client/service/CMakeLists.txt: Support
          omitting tst_libaccounts_service. It is broken on Ubuntu
          24.04 (not so on Debian stable/testing/unstable).
          (fdf1d97)

2024-06-05 Mike Gabriel

        * tests/online-accounts-client/client/ui/: Port to CMake and enable
          at build time. (1390e05)
        * online-accounts-daemon/lib/Lomiri/OnlineAccounts/CMakeLists.txt:
          Drop copying of QML_FILES. No QML files shipped in this
          module. (49ddd5c)
        * tests/online-accounts-client/lib/: Port to CMake and enable at
          build time. (b36b83c)
        *
          tests/online-accounts-client/client/service/tst_ui_proxy/tst_ui_proxy.cpp:
          Silence warning about wrong dir permission for
          XDG_RUNTIME_DIR in testbed. (02f7e34)
        * tests/online-accounts-client/client/service/: Port to CMake and
          enable at build time. (f1f28c3)

2024-06-03 Mike Gabriel

        * tests/online-accounts-client/: Re-sort files/folders. (399d96d)
        * tests/{client,online-accounts-service,online-accounts-ui}/: Blindly
          copy over from l-s-s-online-accounts v0.10. (df38a70)
        * tests/online-accounts-plugins/lib/tst_application_manager.cpp: Fix
          <list>.toSet() deprecations when building against Qt 5.15.
          (47cce61)

2024-06-14 Mike Gabriel

        * tests/online-accounts-plugins/: Port to CMake and enable at build
          time. (5cfc779)

2024-06-03 Mike Gabriel

        * tests/online-accounts-plugins/: Re-arrange files/folders. (c3d79a8)
        * tests/plugin: Blindly copy over plugin unit test from
          l-s-s-online-accounts v0.10. (22469e8)
        * tests/online-accounts-daemon/lib/OnlineAccounts/functional_tests/:
          Run unit test in xvfb-run environment. Test requires
          Xserver. (cfa31ba)
        * tests/online-accounts-daemon/: Run D-Bus related tests in
          dbus-test-runner environment. (960602a)
        * tests/online-accounts-daemon/daemon/CMakeLists.txt: Disable
          tst_daemon. Currently freezing. Needs more investigation.
          (6a9f040)
        * tests/online-accounts-daemon/daemon/functional_tests/: Introduce
          loa-accountd wrapper for capturing accountd activitity
          during unit tests. (1422edf)
        * tests/online-accounts-daemon/daemon/functional_tests/signond.py:
          Bump API mimicking to signond >= 8.60 (introducing
          application contexts). (7f208ee)

2024-06-02 Mike Gabriel

        * tests/online-accounts-daemon/daemon/functional_tests/: Some
          white-space fixes. (479a5b6)
        * tests/online-accounts-daemon/daemon/functional_tests/signond.py:
          Provide more log output during unit test run. (72f7e92)
        * CMakeLists.txt: Use BUILD_TESTING option to enable unit tests.
          (8f485df)

2024-06-14 Mike Gabriel

        * tests: Move daemon/ and lib/ subfolders into
          online-accounts-daemon/ and sort similarly to our code
          folder structure. (3dd04aa)

2024-06-02 Mike Gabriel

        * online-accounts-client/: Port to CMake. Refactor as needed for
          CMake-port. (4ac9a65)
        * online-accounts-plugins/: Port to CMake. Refactor as needed for
          CMake-port. (4d9bf7b)
        * online-accounts-daemon/: Refactor daemon part of
          lomiri-online-accounts. (43539af)
        * CMakeLists.txt: Prepare all Qt libraries we'll need for all aspects
          (client, daemon, plugins) of lomiri-online-accounts.
          (d6babb5)
        * po/: Already move over to the new translation files. (87b0dc2)
        * doc/: Complete API doc rendering. (b731716)
        * CMakeLists.txt: Bump upstream version to 0.12, rename project to
          lomiri-online-accounts. (dce3c7c)
        * debian/: Rework the entire packaging, defining our goals for the
          re-factoring. (86c8d75)
        * online-accounts-daemon/: Drop .2 API version from
          Lomiri.OnlineAccounts (and Ubuntu.OnlineAccounts) QML
          module. (03ea112)
        * Move all library code files into Lomiri-prefixed namespace.
          (06d3204)
        * online-accounts-client/: Identify files used by service, not ui
          (notification.{cpp|h}). (35c9ce7)
        * online-accounts-client/: Identify commonly used code files
          (debug{cpp|h}, globals.h, i18n.{cpp|h}, ipc.{cpp|h}).
          (5742cc5)
        * po/: Blindly copy over localization files from
          l-s-s-online-accounts. (73a9bb9)
        * Rearrange folders/files belonging to plugin lib. (0b4152a)
        * Rearrange folders/files belonging to client lib, service, ui. Move
          to other folders. (eb44049)
        * {client,plugins,online-accounts-ui,online-accounts-service}/:
          Blindly copy these folders/files over from
          l-s-s-online-accounts. (b66a3c0)
        * doc/: Move qdoc files to daemon/ subdir. (39568ad)
        * Move files: src/ -> online-accounts-daemon. (6519118)

2024-03-15 Mike Gabriel

        * Release 0.3 (b20433e) (tag: 0.3)

2023-06-05 Guido Berhoerster

        * Add Lomiri.OnlineAccounts module documentation (7a2c69f)

2022-12-09 Alfred Neumayer

        * daemon: Set assumed AppArmor label for the DBus service (c35f408)

2022-09-20 Guido Berhoerster

        * Modernize packaging (22cb44b)
        * Properly exclude non-packaged files (eb2a92d)
        * Provide Ubuntu compatibility at the source level and add
          deprecation warnings (fcf8865)

2022-09-19 Guido Berhoerster

        * Rename package to lomiri-online-accounts (bfb6a7a)

2021-12-19 Alberto Mardegan

        * debian: add compatibility import for Ubuntu.OnlineAccounts 2.0
          (4ee45af)
        * tests: replace com.ubuntu with com.lomiri (06abcc4)
        * Rename D-Bus service to com.lomiri.OnlineAccounts.Manager (66e0390)
        * Remove bzr configuration files (89ed821)
        * Rename QML packages to Lomiri.OnlineAccounts (7c98b26)
        * debian: update project URLs (ef93ac1)
        * CI: move Jenkinsfile to debian/ (eabf179)

2021-01-12 Marius Gripsgard

        * Update Jenkinsfile (fdb221e)

2021-01-11 Rodney

        * Replace transitional qml dependencies and run tests on arm64 too
          (#2) (8a8fa7b)

2018-11-09 Alberto Mardegan

        * build: Allow building with crossbuilder (#1) (7e0b54e)

2018-01-09 Dan Chapman

        * Import to UBports (7e6ef1c)

2016-12-23 Bileto Bot

        * Releasing 0.1+17.04.20161223-0ubuntu1 (c758f48)

2016-12-23 Alberto Mardegan

        * Tests: run main loop to avoid memory leaks (LP: #1617180) (55419cc)

2016-12-23 Michi Henning

        * Fixed memory leak in PendingCallWatcher. Got rid of compiler
          warning. Added missing \endlist to documentation. (LP:
          #1617180) (f6c6b53)

2016-12-21 Alberto Mardegan

        * same for QML tests (eb49286)
        * Move wait to cleanup method (53865a0)

2016-12-21 Michi Henning

        * Added include for QScopedPointer. (02218b5)

2016-12-21 Alberto Mardegan

        * Tests: allow cleanup functions to run (2709571)

2016-12-21 Michi Henning

        * Use QScopedPointer. (78c4b1e)
        * Fixed memory leak in PendingCallWatcher. Got rid of compiler
          warning. Added missing \endlist to documentation.
          (df4cfd3)

2016-11-22 Bileto Bot

        * Releasing 0.1+17.04.20161122.1-0ubuntu1 (ea1240b)
        * debian/control:   - libonline-accounts-qt1 should depend on the
          service-side library of the     same version, and
          recommend online-accounts-daemon (LP: #1643421) (6b65f46)

2016-11-22 Alberto Mardegan

        * libonline-accounts-qt1 should depend on the service-side library of
          the same version, and recommend online-accounts-daemon
          (LP: #1643421) (f2cf8e4)
        * debian/control:   - libonline-accounts-qt1 recommends
          online-accounts-daemon (LP: #1643421) (ab9fd49)

2016-11-10 Bileto Bot

        * Releasing 0.1+17.04.20161110-0ubuntu1 (49a5e50)

2016-11-10 Alberto Mardegan

        * Add provider information to public API (9c0af83)
        * Remove commented line (f13f342)

2016-11-09 Alberto Mardegan

        * Expose enums to QML (233bfa8)
        * Skip missing providers (b5db507)
        * Don't crash if serviceList property is retrieved when the model is
          not ready (986d81f)
        * remove debugging (8bdff16)

2016-11-08 Alberto Mardegan

        * fix test (83c7e5b)
        * test debugging (782ffcb)
        * Fallback to provider's icon and display name (09c23c8)

2016-11-07 Alberto Mardegan

        * fix (f6802e0)
        * skip test (ed2812a)
        * fix (636529f)
        * type conv for qt 5.4 (b73c6a9)
        * Fix typo (0ea6765)
        * Don't use Q_GADGET functionality in Qt < 5.5 (3be2c02)
        * Cast QList<QString> to QStringList (bb5f546)
        * more of the same (3c34287)
        * DOn't use qjsEngine() it doesn't exist in QT 5.4 (6810274)
        * Add missing files (d0d5384)

2016-11-04 Alberto Mardegan

        * Bump dependency on libaccounts-qt (a426d9a)
        * Add service property to account (f7da457)

2016-11-03 Alberto Mardegan

        * remove translation, add icon (bac8cce)
        * From trunk (73b546f)

2016-11-01 Bileto Bot

        * Releasing 0.1+17.04.20161101-0ubuntu1 (7a21e58)

2016-11-01 Alberto Mardegan

        * Disable debug output by default (4cd5b2a)
        * Disable debug messages by default (09d6d11)

2016-10-14 Alberto Mardegan

        * Add missing files (9dc42f7)
        * Add availableServices to client Qt API (6b05d74)

2016-10-13 Bileto Bot

        * Releasing 0.1+16.10.20161013.1-0ubuntu1 (d04ea89)

2016-10-13 Alberto Mardegan

        * Use GetConnectionCredentials() method instead of the deprecated
          apparmor-specific method. (LP: #1489489) (494bec3)
        * Merge from trunk;  Fix reply of password-based authentication (LP:
          #1628473);  debian/control:   - Depend on
          qttools5-dev-tools for qdoc (LP: #1608814);  debian/rules:
            - No need to invoke dh_python (3421b5e)

2016-10-11 Alberto Mardegan

        * Deliver service list with GetAccounts (f8c7ea8)

2016-10-06 Alberto Mardegan

        * Return services, not providers (63a819c)

2016-10-06 Bileto Bot

        * Releasing 0.1+16.10.20161006.3-0ubuntu1 (5fa3daf)

2016-10-06 Alberto Mardegan

        * Fix reply of password-based authentication (4cbc499)
        * Update bug number in comment (8f34502)
        * Temporarily skip tests on arm64 (ec61e97)
        * Make QML test more robust (8e6ccac)
        * From trunk (4122b29)

2016-10-05 Alberto Mardegan

        * Implement the server-side functionality (86fbd0b)

2016-10-04 Alberto Mardegan

        * Use new DBus API (261fafe)

2016-10-03 Bileto Bot

        * Releasing 0.1+16.10.20161003.1-0ubuntu1 (0895f5d)
        * debian/control:   - Depend on qttools5-dev-tools for qdoc (LP:
          #1608814);  debian/rules:   - No need to invoke dh_python
          (434dbee)

2016-10-03 Alberto Mardegan

        * Remove upstart dep (edb7548)

2016-09-29 Alberto Mardegan

        * Build-Depend on upstart to avoid s390x builds on yakkety. (60ffb37)

2016-09-28 Alberto Mardegan

        * Rename keys in password reply (ea1e9cc)

2016-08-17 Alberto Mardegan

        * Packaging fixes (b220278)

2016-07-22 Bileto Bot

        * Releasing 0.1+16.10.20160722.4-0ubuntu1 (23fc8bc)

2016-07-22 Alberto Mardegan

        * Enable coverage reporting for the OnlineAccountsDaemon library
          (8514863)
        * Tests: keep the service alive while running the test (9d72d09)

2016-07-22 James Henstridge

        * Expose all serviceIds for each account rather than ignoring all but
          the first. (LP: #1602159) (3e0f7f7)
        * Add a constructor for OnlineAccounts::Manager that takes a custom
          D-Bus connection. (LP: #1602153) (e8f3f01)

2016-07-20 Alberto Mardegan

        * Enable coverage for the OnlineAccountsDaemon library (15052bf)
        * Tests: restart the D-Bus session with each test (14ce072)

2016-07-19 Alberto Mardegan

        * Revert (71bfc46)

2016-07-18 Alberto Mardegan

        * Tests: prevent the service from quitting (e77e241)

2016-07-16 James Henstridge

        * Fix typo. (5885b59)
        * Update a few tests to include some extra debug information on
          failure. (5d10649)

2016-07-12 James Henstridge

        * Update multiple services test to properly exercise the "any
          serviceId" case. (87252b7)
        * Update symbols file. (4fa4e36)
        * Add a test for the multiple services tied to a single account case.
          (9ace735)
        * Fix tst_qml_module test. (1d98dd0)
        * Fix functional_tests. (fe3c12e)
        * Make m_accounts use (accountId, serviceId) as a key. (0d50564)
        * Update symbols file. (3b67f16)
        * Add a second constructor for the Manager class that takes a custom
          QDBusConnection. (f5471e1)

2016-06-09 Bileto Bot

        * Releasing 0.1+16.10.20160609-0ubuntu1 (9d766ae)

2016-06-09 Alberto Mardegan

        * Add symbols files (LP: #1548693) (8d7fa6b)

2016-02-25 Alberto Mardegan

        * Add symbols files (c5e06b4)

2016-02-12 CI Train Bot

        * Releasing 0.1+16.04.20160212-0ubuntu1 (24fbea6)

2016-02-12 Alberto Mardegan

        * Add a property to signal when the account model is ready.
          Fixes:
          #1513075 (c8c80b2)
        * Add support for SASL authentication.
          Fixes: #1519330 (dab71dc)
        * Revert last commit, it was needed only for debugging. (ff31409)

2016-02-11 Alberto Mardegan

        * debugging (43fb0f8)
        * Add invalidation for password and sasl plugins (d727d56)
        * Rename parameters: make them consistent with the SASL plugin
          (7be60a0)

2016-01-18 Alberto Mardegan

        * Add a property to signal when the account model is ready. (a90d759)

2016-01-13 Alberto Mardegan

        * Add support for SASL authentication (313f032)

2015-11-03 CI Train Bot

        * Releasing 0.1+16.04.20151103.1-0ubuntu1 (1976e57)

2015-11-03 Alberto Mardegan

        * If the applicationId is not set, force using APP_ID (3c2ffa9)
        * Tests (8c004cc)
        * Pass auth data back to the client (874160a)

2015-10-30 Alberto Mardegan

        * If the applicationId is not set, force using APP_ID (a5476d0)

2015-09-23 CI Train Bot

        * Releasing 0.1+15.10.20150923.1-0ubuntu1 (fbd8154)

2015-09-23 Alberto Mardegan

        * Docs, fixes, library install (a4ada38)
        * Also read provider settings (3519435)
        * Add get() method to model (6a5809f)

2015-09-04 Alberto Mardegan

        * Install .so file (c3208e9)
        * Install .so file (3801869)

2015-09-03 Alberto Mardegan

        * Hide library symbols (aa3f230)
        * Hide symbols from libs (a6be931)
        * as library (b8d121d)
        * fixes (ec439a8)
        * from trunk (2e039c8)

2015-09-02 Alberto Mardegan

        * Add C method (ff7e7df)
        * remove private include file from public include (0d55bcb)
        * Packaging (770f83a)

2015-08-28 Alberto Mardegan

        * pkgconfi (5cf004e)
        * fix dep (dd1d9e8)
        * Make the daemon available as a library (73f3449)

2015-08-27 Alberto Mardegan

        * describe special authentication parameters (6eb7524)
        * Clean auth data (403f83e)

2015-08-26 Alberto Mardegan

        * Use new API in QML module (8611b1b)
        * Add methods to access the authentication data as a dictionary
          (ef6c76c)
        * remove leftover commented code (f29ac4b)
        * Use the new Account::keys() API in the QML module (cfd5997)
        * Add API to retrieve the list of setting keys (7522e6f)

2015-08-20 CI Train Bot

        * Releasing 0.1+15.10.20150820-0ubuntu1 (95975fd)

2015-08-20 Alberto Mardegan

        * Implement the D-Bus service (7035141)
        * Reduce usage of "static" (05e4b0e)
        * Remove unneeded "mutable" keywords (11d103d)

2015-08-19 Alberto Mardegan

        * packaging (acbfa16)
        * Document error codes (4eb0c55)

2015-08-12 Alberto Mardegan

        * Add documentation (a405674)

2015-08-06 Alberto Mardegan

        * Add QML tests (db0a553)
        * Add tests to QML module (fc548ff)

2015-08-04 Alberto Mardegan

        * fix tests (5c8915e)
        * add missing dependency (58ff00f)
        * Add QML API (1b8dba2)
        * QML doesn't handle bytearrays (7f0a785)

2015-08-03 Alberto Mardegan

        * sessions are interactive by default (6c2225a)
        * Don't fill secrets if not given (a5212da)
        * fix insert rows (cefb3e4)
        * Pass client pid to Setup (460a3ac)

2015-07-31 Alberto Mardegan

        * Add QML API (eefdebb)

2015-07-23 Alberto Mardegan

        * Do not call aa_is_enabled() (8c1c79e)
        * Revert all debugging changes (93fc258)
        * debug (f400292)
        * debug (efb92ea)
        * debug (6b5a82e)
        * Debug help (7fda795)
        * Add debug info (f5c559c)

2015-07-22 Alberto Mardegan

        * Tests: don't log to xml (2243174)

2015-07-21 Alberto Mardegan

        * Install header files in the correct directory (92afa16)

2015-07-17 Alberto Mardegan

        * Bump dependency (e93f9e2)

2015-07-16 Alberto Mardegan

        * Remove gmock (c6881ac)
        * bootstrap citrain (2bb5dbc)

2015-07-15 Alberto Mardegan

        * merge trunk (63e96ff)

2015-07-10 Alberto Mardegan

        * Test account changes (74598d2)
        * Timer: stop timer when objects are busy (63665eb)
        * Quit when disconnected (73cd691)
        * Watch for new accounts (65b0bfb)
        * Add changelog (d7df985)

2015-07-09 Alberto Mardegan

        * Save state when quitting (dadd4ac)

2015-07-08 Alberto Mardegan

        * Test apparmor interaction (7e08d4c)
        * Fix path of daemon (b1991c3)
        * Debian packaging (7a31090)
        * Packaging (33cb137)

2015-07-07 Alberto Mardegan

        * Allow faking the apparmor profile (5d65622)
        * mock: do not register the same object twice (04abd44)
        * Do not quit when an operation is in progress (fbe3b2e)

2015-07-06 Alberto Mardegan

        * Test authentication (3c04a17)

2015-07-02 Alberto Mardegan

        * setup testing the libaccounts part (d677343)

2015-07-01 Alberto Mardegan

        * Skeleton of daemon tests (a474f7a)

2015-06-29 Alberto Mardegan

        * Fix marshalling for RequestAccess reply (e890515)

2015-06-24 Alberto Mardegan

        * Fix registration of account info (609b80c)
        * Create dir if not existing (23e966e)
        * Make timeout configurable (cbe6b52)
        * Implement access request (61b5f0f)

2015-06-16 Alberto Mardegan

        * Split authenticator out of the requests (8bee605)

2015-06-12 Alberto Mardegan

        * Use AsyncOperation class (d1a03dc)

2015-06-10 Alberto Mardegan

        * wip (21e1e56)
        * Use proper error code (4af25bb)
        * Use Authenticator class (aae100d)
        * Add authenticator class (1e6078e)

2015-06-09 Alberto Mardegan

        * Watch account changes (4938f5d)
        * WIP (eb7868e)

2015-06-03 Alberto Mardegan

        * monitor clients (eb0fadd)
        * WIP (03a8435)

2015-05-28 Alberto Mardegan

        * WIP (ef14193)
        * WIP (53a051e)

2015-05-26 Alberto Mardegan

        * More WIP (ceaa5b7)

2015-05-22 Alberto Mardegan

        * WIP (61127c5)
        * Add InactivityTimer class (2a5b5dc)
        * Headers and D-Bus disconnection (fabe990)

2015-04-28 Alberto Mardegan

        * Update DBus API (2d487f1)
        * Qt client API implementation (95b710c)

2015-03-11 Alberto Mardegan

        * Rename library to OnlineAccountsQt (bb5e9a3)

2015-03-04 Alberto Mardegan

        * Test authentication errors (1635da0)
        * Rename project (1045e12)
        * More coverage (ae75096)
        * More test coverage, handle signal (061a6de)

2015-03-03 Alberto Mardegan

        * Fix handling of AccountChanged signalFix handling of AccountChanged
          signalFix handling of AccountChanged signalFix handling of
          AccountChanged signalFix handling of AccountChanged
          signalFix handling of AccountChanged signalFix handling of
          AccountChanged signalFix handling of AccountChanged
          signalFix handling of AccountChanged signal (9c5185a)

2015-03-02 Alberto Mardegan

        * Fix build and tests (0cf41d9)

2015-02-27 Alberto Mardegan

        * more tests, WIP (45582c2)
        * Begin functional testing, add coverage reporting (f3aa79c)
        * Add public headers (63f634b)
        * Tests: tst_authentication_data (3b7969d)
        * Implement missing methods, warn on undefined symbols (c143970)
        * Implement RequestAccessReply (07bb077)

2015-02-26 Alberto Mardegan

        * some work on account.cpp (545cc52)
        * Authentication data (4f5993c)
        * Implement part of Manager (d0d5ea0)

2015-02-25 Alberto Mardegan

        * Merge QT api (255db7e)
        * instantiate the interface (7a3561a)
        * Add Manager::ready() signal (65f18ef)
        * signal and metatype (de44bc1)
        * Basic DBus interface (bb3de92)
        * merge from qt api (ff9a713)
        * Implement AuthenticationReply (8404d65)
        * Change construction of accounts (a49fb4a)
        * AccountAvailable carries the Account itself (914fe6a)
        * Use byte array for access token (9d9c082)

2015-02-24 Alberto Mardegan

        * Rename signal to accountAvailable() (5a322ee)
        * Add RequestAccessReply class (0f8ed11)

2015-02-23 Alberto Mardegan

        * Revert QFuture usage (e5ecea0)

2015-02-20 Alberto Mardegan

        * Add AuthenticationReply::hasError() (9edd878)
        * Review comments: (9153a5d)

2015-02-19 Alberto Mardegan

        * Add Error::isValid() (f808e52)
        * Complete declarations for Qt API (efb3b6c)

2015-02-18 Alberto Mardegan

        * Update docstring (b65137d)
        * from trunk (a9c3559)
        * style (f297593)

2015-02-12 Alberto Mardegan

        * sketch (bcc4114)

2015-02-11 Alberto Mardegan

        * merge style (20cfa2a)
        * style (dce2ae0)
        * From the dbus branch (d7456d8)
        * Update (43d444b)
        * merge from style (84f64b3)
        * Review updates (5ce21b7)

2015-02-11 Marcus Tomlinson

        * Added the testing structure (9621c91)
        * Some formatting (88c973d)
        * Added testing (4d31b65)

2015-02-10 Alberto Mardegan

        * Update (5d497c9)
        * Remove mutable (e11a2b6)
        * Style fixes (b36afcd)
        * Add AccountInfo class to group the account ID plus details
          (1707412)

2015-02-10 James Henstridge

        * Remove some unnecessary debug messages. (098e80c)
        * Update interface docs. (2b3f660)
        * Add AccountInfo class to group the account ID plus details, and
          make GetAccounts() return this structure. (054e2ac)

2015-02-09 Alberto Mardegan

        * Update D-Bus interface, fix warnings (4e8e2e9)

2015-02-09 James Henstridge

        * Add checkAccess() to check for access to a particular service ID.
          (847c06d)
        * Start of AppArmor policy checking code. (929889a)

2015-02-06 James Henstridge

        * Skeleton for accounts daemon. (0e9fa78)
