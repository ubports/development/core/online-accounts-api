project(LomiriOnlineAccountsPluginQML LANGUAGES CXX)

pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
    ${LomiriOnlineAccountsPlugin_SOURCE_DIR}/..
)

set(PLUGIN_MODULE LomiriOnlineAccountsPluginQML)
set(API_VER 1.0)
set(API_URI Lomiri.OnlineAccounts.Plugin)

add_definitions(
    ${ACCOUNTSQT_CFLAGS}
)

add_library(${PLUGIN_MODULE} MODULE
    plugin.cpp
    qml-loopback-server.cpp
)

set_target_properties(${PLUGIN_MODULE} PROPERTIES
    LINK_FLAGS -Wl,--no-undefined
)

target_link_libraries(${PLUGIN_MODULE}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::Qml
    Qt${QT_VERSION}::Quick
    lomiri-online-accounts-plugin
    ${ACCOUNTSQT_LDFLAGS}
)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/qmldir.in ${CMAKE_CURRENT_BINARY_DIR}/qmldir @ONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config.hpp.in ${CMAKE_CURRENT_BINARY_DIR}/config.hpp @ONLY)

set(QML_FILES_PLUGIN_MODULE
    ChromedWebView.qml
    DuplicateAccount.qml
    ErrorItem.qml
    KeyboardRectangle.qml
    OAuthMain.qml
    OAuth.qml
    Options.qml
    RemovalConfirmation.qml
    ServiceItem.qml
    ServiceItemBase.qml
    ServiceSwitches.qml
    StandardAnimation.qml
    WebView.qml
)

# Module install

set(PLUGIN_IMPORTS_DIR "${QT_INSTALL_QML}/Lomiri/OnlineAccounts/Plugin")
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/qmldir DESTINATION ${PLUGIN_IMPORTS_DIR})
install(TARGETS ${PLUGIN_MODULE} DESTINATION ${PLUGIN_IMPORTS_DIR})
install(FILES ${QML_FILES_PLUGIN_MODULE} DESTINATION ${PLUGIN_IMPORTS_DIR})

if(NOT ${CMAKE_CURRENT_BINARY_DIR} STREQUAL ${CMAKE_CURRENT_SOURCE_DIR})
  # copy qml files over to build dir to be able to import them uninstalled
  foreach(_file ${QML_FILES_PLUGIN_MODULE})
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${_file}
                       DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_file}
                       COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_SOURCE_DIR}/${_file} ${CMAKE_CURRENT_BINARY_DIR}/${_file})
  endforeach(_file)
  add_custom_target(${PLUGIN_MODULE}_copy_files_to_build_dir DEPENDS ${QML_FILES_PLUGIN_MODULE})
  add_dependencies(${PLUGIN_MODULE} ${PLUGIN_MODULE}_copy_files_to_build_dir)
endif()
