/*
 * Copyright (C) 2014 Canonical Ltd.
 *
 * Contact: Alberto Mardegan <alberto.mardegan@canonical.com>
 *
 * This file is part of OnlineAccountsPlugin.
 *
 * OnlineAccountsClient is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * OnlineAccountsClient is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with OnlineAccountsClient.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "plugin.h"

#include "config.hpp"
#include "qml-loopback-server.h"

#include <libintl.h>
#include <LomiriOnlineAccountsPlugin/application-manager.h>
#include <LomiriOnlineAccountsPlugin/request-handler.h>
#include <QDebug>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQmlEngine>

using namespace OnlineAccountsPlugin;

void Plugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_UNUSED(uri);

    std::string localeDir = i18nDirectory().toStdString();
    bindtextdomain("lomiri-online-accounts", localeDir.c_str());
    bind_textdomain_codeset("lomiri-online-accounts", "UTF-8");

    QQmlContext* context = engine->rootContext();
    context->setContextProperty("ApplicationManager",
                                OnlineAccountsUi::ApplicationManager::instance());
}

void Plugin::registerTypes(const char *uri)
{
    qDebug() << Q_FUNC_INFO << uri;

    qmlRegisterType<SignOnUi::RequestHandler>(uri, 1, 0, "RequestHandler");
    qmlRegisterType<QmlLoopbackServer>(uri, 1, 0, "LoopbackServer");
}
