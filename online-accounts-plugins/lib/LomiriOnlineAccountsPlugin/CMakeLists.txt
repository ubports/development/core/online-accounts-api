project(LomiriOnlineAccountsPlugin LANGUAGES CXX)

pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)
pkg_check_modules(SIGNONPLUGINSCOMMON signon-plugins-common REQUIRED)

set(PLUGIN_LIB lomiri-online-accounts-plugin)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${LomiriOnlineAccountsQt_SOURCE_DIR}/..
)

add_definitions(
    ${ACCOUNTSQT_CFLAGS}
    ${SIGNONPLUGINSCOMMON_CFLAGS}
)

add_definitions(-DONLINE_ACCOUNTS_PLUGIN_DIR="${ONLINE_ACCOUNTS_PLUGIN_DIR}")
add_definitions(-DBUILDING_ONLINE_ACCOUNTS_PLUGIN)

set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--no-undefined")
set(CMAKE_CXX_VISIBILITY_PRESET hidden)

add_library(${PLUGIN_LIB} SHARED
    account-manager.cpp
    application-manager.cpp
    loopback-server.cpp
    request-handler.cpp
)
set_target_properties(${PLUGIN_LIB} PROPERTIES
    VERSION 1.0.0
    SOVERSION 1
    LINK_FLAGS -Wl,--no-undefined
)
target_link_libraries(${PLUGIN_LIB}
    Qt${QT_VERSION}::Gui
    Qt${QT_VERSION}::Network
    ${ACCOUNTSQT_LDFLAGS}
    ${SIGNONPLUGINSCOMMON_LDFLAGS}
)

set_target_properties(${PLUGIN_LIB} PROPERTIES AUTOMOC TRUE)

# Library install

install(TARGETS ${PLUGIN_LIB} LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

# Development files

configure_file(${PROJECT_NAME}.pc.in ${PROJECT_NAME}.pc @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pc DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)
