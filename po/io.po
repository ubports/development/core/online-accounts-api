# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd. 
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-online-accounts-client\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-10 12:19+0200\n"
"PO-Revision-Date: 2023-01-05 10:50+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Ido <https://hosted.weblate.org/projects/lomiri/lomiri-online-"
"accounts-client/io/>\n"
"Language: io\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: online-accounts-ui/src/qml/SignOnUiDialog.qml:91
#: online-accounts-ui/src/qml/SignOnUiPage.qml:32
#: online-accounts-plugins/lib/module/OAuth.qml:170
#: online-accounts-plugins/lib/module/RemovalConfirmation.qml:43
#, fuzzy
msgid "Cancel"
msgstr "Cancel"

#: online-accounts-ui/src/qml/AuthorizationPage.qml:87
#, qt-format
msgid "wants to access your %2 account"
msgstr ""

#: online-accounts-ui/src/qml/AuthorizationPage.qml:131
msgid "Allow"
msgstr ""

#: online-accounts-ui/src/qml/AuthorizationPage.qml:140
msgid "Add another account…"
msgstr ""

#: online-accounts-ui/src/qml/AuthorizationPage.qml:148
msgid "Don't allow"
msgstr ""

#: online-accounts-plugins/lib/module/Options.qml:36
msgid "ID"
msgstr ""

#: online-accounts-plugins/lib/module/Options.qml:48
msgid "Remove account…"
msgstr ""

#: online-accounts-plugins/lib/module/ErrorItem.qml:36
msgid "This service is not available right now. Try again later."
msgstr ""

#: online-accounts-plugins/lib/module/ErrorItem.qml:43
msgid "Try Again"
msgstr ""

#: online-accounts-plugins/lib/module/OAuth.qml:131
msgid "Loading…"
msgstr ""

#: online-accounts-plugins/lib/module/RemovalConfirmation.qml:31
msgid "Remove account"
msgstr ""

#: online-accounts-plugins/lib/module/RemovalConfirmation.qml:32
#, qt-format
msgid ""
"The %1 account will be removed only from your phone. You can add it again "
"later."
msgstr ""

#: online-accounts-plugins/lib/module/RemovalConfirmation.qml:37
msgid "Remove"
msgstr ""

#: online-accounts-plugins/lib/module/ServiceSwitches.qml:33
msgid "Access to this account:"
msgstr ""

#: online-accounts-plugins/lib/module/DuplicateAccount.qml:31
msgid "Duplicate account"
msgstr ""

#: online-accounts-plugins/lib/module/DuplicateAccount.qml:32
msgid "There is already an account created for this email address."
msgstr ""

#: online-accounts-plugins/lib/module/DuplicateAccount.qml:37
msgid "OK"
msgstr ""
