project(LomiriOnlineAccountsService LANGUAGES CXX)

pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)
pkg_check_modules(SIGNONQT libsignon-qt${QT_VERSION} REQUIRED)
pkg_check_modules(APPARMOR libapparmor REQUIRED)
pkg_check_modules(GLIB20 glib-2.0 REQUIRED)
pkg_check_modules(NOTIFY libnotify REQUIRED)
pkg_check_modules(SIGNONPLUGINSCOMMON signon-plugins-common REQUIRED)

set(LOA_SERVICE lomiri-online-accounts-service)
set(LOA_UI lomiri-online-accounts-ui)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_SOURCE_DIR}
    ${LomiriOnlineAccountsDaemon_SOURCE_DIR}/..
)

add_definitions(
    ${ACCOUNTSQT_CFLAGS}
    ${SIGNONQT_CFLAGS}
    ${APPARMOR_CFLAGS}
    ${GLIB20_CFLAGS}
    ${NOTIFY_CFLAGS}
    ${SIGNONPLUGINSCOMMON_CFLAGS}
)

add_definitions(-DSIGNONUI_I18N_DOMAIN="${SIGNONUI_I18N_DOMAIN}")
add_definitions(-DDEBUG_ENABLED)
add_definitions(-DLOA_UI="${LOA_UI}")
add_definitions(-DINSTALL_BIN_DIR="${CMAKE_INSTALL_FULL_BINDIR}")
add_definitions(-DQT_NO_KEYWORDS)

set(LOA_SERVICE_SOURCES
    inactivity-timer.cpp
    indicator-service.cpp
    libaccounts-service.cpp
    main.cpp
    notification.cpp
    reauthenticator.cpp
    request.cpp
    request-manager.cpp
    service.cpp
    signonui-service.cpp
    ui-proxy.cpp
    utils.cpp
)

set(LOA_SERVICE_HEADERS
    inactivity-timer.h
    indicator-service.h
    libaccounts-service.h
    notification.h
    reauthenticator.h
    request.h
    request-manager.h
    service.h
    signonui-service.h
    ui-proxy.h
    utils.h
)

set(DBUS_ADAPTOR_SOURCES
    onlineaccountsuiadaptor.cpp
    webcredentialsadaptor.cpp
)
set(DBUS_ADAPTOR_HEADERS
    onlineaccountsuiadaptor.h
    webcredentialsadaptor.h
)

qt_add_dbus_adaptor(DBUS_ADAPTOR_SOURCES com.lomiri.OnlineAccountsUi.xml service.h OnlineAccountsUi::Service)
qt_add_dbus_adaptor(DBUS_ADAPTOR_SOURCES com.canonical.indicators.webcredentials.xml indicator-service.h SignOnUi::IndicatorService)

set (COMMON_SOURCES
    ${CMAKE_SOURCE_DIR}/online-accounts-common/debug.cpp
    ${CMAKE_SOURCE_DIR}/online-accounts-common/i18n.cpp
    ${CMAKE_SOURCE_DIR}/online-accounts-common/ipc.cpp
)
set (COMMON_HEADERS
    ${CMAKE_SOURCE_DIR}/online-accounts-common/debug.h
    ${CMAKE_SOURCE_DIR}/online-accounts-common/globals.h
    ${CMAKE_SOURCE_DIR}/online-accounts-common/i18n.h
    ${CMAKE_SOURCE_DIR}/online-accounts-common/ipc.h
)

if (ENABLE_MIRCLIENT)
    pkg_search_module(MIRCLIENT REQUIRED mir1client mirclient)
    add_definitions(${MIRCLIENT_CFLAGS})
    set(MIRCLIENT_SOURCES
        mir-helper.cpp
        mir-helper.h
    )
else()
    set(MIRCLIENT_SOURCES
        mir-helper-stub.cpp
        mir-helper.h
    )
    set(MIRCLIENT_LDFLAGS "")
endif()


add_executable(${LOA_SERVICE}
    ${COMMON_SOURCES}
    ${COMMON_HEADERS}
    ${DBUS_ADAPTOR_SOURCES}
    ${DBUS_ADAPTOR_HEADERS}
    ${MIRCLIENT_SOURCES}
    ${LOA_SERVICE_SOURCES}
    ${LOA_SERVICE_HEADERS}
)
target_link_libraries(${LOA_SERVICE}
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Network
    Qt${QT_VERSION}::Xml
    lomiri-online-accounts-daemon
    ${ACCOUNTSQT_LDFLAGS}
    ${SIGNONQT_LDFLAGS}
    ${APPARMOR_LDFLAGS}
    ${GLIB20_LDFLAGS}
    ${NOTIFY_LDFLAGS}
    ${SIGNONPLUGINSCOMMON_LDFLAGS}
    ${MIRCLIENT_LDFLAGS}
)

set_target_properties(${LOA_SERVICE} PROPERTIES AUTOMOC TRUE)

install(TARGETS ${LOA_SERVICE}
        RUNTIME DESTINATION ${CMAKE_INSTALL_FULL_BINDIR}
)

set(LOA_SERVICE_SERVICE_FILE com.lomiri.OnlineAccounts.Manager.service)
configure_file(${LOA_SERVICE_SERVICE_FILE}.in ${CMAKE_CURRENT_BINARY_DIR}/${LOA_SERVICE_SERVICE_FILE} @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${LOA_SERVICE_SERVICE_FILE}
    DESTINATION ${CMAKE_INSTALL_FULL_DATADIR}/dbus-1/services/
)

set(LOA_UI_SERVICE_FILE com.lomiri.OnlineAccountsUi.service)
configure_file(${LOA_UI_SERVICE_FILE}.in ${CMAKE_CURRENT_BINARY_DIR}/${LOA_UI_SERVICE_FILE} @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${LOA_UI_SERVICE_FILE}
        DESTINATION ${CMAKE_INSTALL_FULL_DATADIR}/dbus-1/services/
)

set(DBUSIFACEDIR "${CMAKE_INSTALL_DATADIR}/dbus-1/interfaces/")

install(FILES
        com.lomiri.OnlineAccountsUi.xml
        DESTINATION ${DBUSIFACEDIR}
)

install(FILES
        com.canonical.indicators.webcredentials.xml
        DESTINATION ${DBUSIFACEDIR}
)
