set(TEST tst_client)
set(SOURCES
    tst_client.cpp
)

add_executable(${TEST} ${SOURCES})
include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}
    ${LomiriOnlineAccountsClient_SOURCE_DIR}/..
    ${LomiriOnlineAccountsUi_SOURCE_DIR}
)

target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Test
    lomiri-online-accounts-client
)

add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
