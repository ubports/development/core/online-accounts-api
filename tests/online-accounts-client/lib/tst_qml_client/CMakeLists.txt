include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}
    ${LomiriOnlineAccountsUi_SOURCE_DIR}/../common
)

# tst_qml_client

set(TEST tst_qml_client)
set(SOURCES
    tst_qml_client.cpp
)

add_definitions(-DPLUGIN_PATH="${CMAKE_CURRENT_BINARY_DIR}")
add_executable(${TEST} ${SOURCES})

target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Qml
    Qt${QT_VERSION}::Test
    lomiri-online-accounts-client
)

# Prepare QML module loading test bed...
add_dependencies(${TEST} LomiriOnlineAccountsClientQML)
add_custom_command(
        TARGET ${TEST} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                ${LomiriOnlineAccountsClientQML_BINARY_DIR}/qmldir
                ${CMAKE_CURRENT_BINARY_DIR}/Lomiri/OnlineAccounts/Client/qmldir
        COMMAND ${CMAKE_COMMAND} -E copy
                ${LomiriOnlineAccountsClientQML_BINARY_DIR}/libLomiriOnlineAccountsClientQML.so
                ${CMAKE_CURRENT_BINARY_DIR}/Lomiri/OnlineAccounts/Client/libLomiriOnlineAccountsClientQML.so)

add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
