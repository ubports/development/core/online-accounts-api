include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}
    ${LomiriOnlineAccountsService_SOURCE_DIR}
)

# tst_inactivity_timer

set(TEST tst_inactivity_timer)
set(SOURCES
    tst_inactivity_timer.cpp
    ${LomiriOnlineAccountsService_SOURCE_DIR}/inactivity-timer.cpp
)
add_executable(${TEST} ${SOURCES})
target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::Test
)
add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
