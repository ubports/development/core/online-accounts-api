pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)
pkg_check_modules(APPARMOR libapparmor REQUIRED)
pkg_check_modules(SIGNONPLUGINSCOMMON signon-plugins-common REQUIRED)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_SOURCE_DIR}
    ${LomiriOnlineAccountsService_SOURCE_DIR}
    ${ACCOUNTSQT_INCLUDE_DIRS}
    ${APPARMOR_INCLUDE_DIRS}
    ${SIGNONPLUGINSCOMMON_INCLUDE_DIRS}
)

# tst_service

set(TEST tst_service)
set(SOURCES
    ${CMAKE_CURRENT_BINARY_DIR}/onlineaccountsuiadaptor.cpp
    ${CMAKE_CURRENT_BINARY_DIR}/onlineaccountsuiadaptor.h
    ${LomiriOnlineAccountsService_SOURCE_DIR}/request.cpp
    ${LomiriOnlineAccountsService_SOURCE_DIR}/request.h
    ${LomiriOnlineAccountsService_SOURCE_DIR}/request-manager.cpp
    ${LomiriOnlineAccountsService_SOURCE_DIR}/request-manager.h
    ${LomiriOnlineAccountsService_SOURCE_DIR}/service.cpp
    ${LomiriOnlineAccountsService_SOURCE_DIR}/service.h
    ${LomiriOnlineAccountsService_SOURCE_DIR}/ui-proxy.h
    ${LomiriOnlineAccountsService_SOURCE_DIR}/utils.cpp
    ${LomiriOnlineAccountsService_SOURCE_DIR}/utils.h
    tst_service.cpp
)

add_definitions(-DNO_REQUEST_FACTORY)

qt_add_dbus_adaptor(SOURCES ${LomiriOnlineAccountsService_SOURCE_DIR}/com.lomiri.OnlineAccountsUi.xml service.h OnlineAccountsUi::Service)

add_executable(${TEST} ${SOURCES})
target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Test
    Qt${QT_VERSION}::Xml
    ${ACCOUNTSQT_LIBRARIES}
    ${APPARMOR_LIBRARIES}
    ${SIGNONPLUGINSCOMMON_LIBRARIES}
)
set_target_properties(${TEST} PROPERTIES AUTOMOC TRUE)
add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})

