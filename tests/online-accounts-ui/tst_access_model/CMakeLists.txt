pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}
    ${LomiriOnlineAccountsUi_SOURCE_DIR}
    ${LomiriOnlineAccountsUi_SOURCE_DIR}/../common
    ${LomiriOnlineAccountsPlugin_SOURCE_DIR}/..
    ${ACCOUNTSQT_INCLUDE_DIRS}
)

# tst_access_model

set(TEST tst_access_model)
set(SOURCES
    tst_access_model.cpp
    ${LomiriOnlineAccountsUi_SOURCE_DIR}/access-model.cpp
    ${CMAKE_SOURCE_DIR}/online-accounts-common/debug.cpp
)

add_definitions(-DPLUGIN_PATH="${CMAKE_CURRENT_BINARY_DIR}")
add_definitions(-DTEST_DATA_DIR="${CMAKE_CURRENT_SOURCE_DIR}/../data")

add_executable(${TEST} ${SOURCES})
target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Qml
    Qt${QT_VERSION}::Test
    lomiri-online-accounts-plugin
    ${ACCOUNTSQT_LIBRARIES}
)

# Prepare QML module loading test bed...
add_dependencies(${TEST} LomiriOnlineAccountsQML)
add_custom_command(
        TARGET ${TEST} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                ${LomiriOnlineAccountsQML_BINARY_DIR}/qmldir
                ${CMAKE_CURRENT_BINARY_DIR}/Lomiri/OnlineAccounts/qmldir
#        COMMAND ${CMAKE_COMMAND} -E copy
#                ${LomiriOnlineAccountsQML_BINARY_DIR}/plugin.qmltypes
#                ${CMAKE_CURRENT_BINARY_DIR}/Lomiri/OnlineAccounts/plugin.qmltypes
        COMMAND ${CMAKE_COMMAND} -E copy
                ${LomiriOnlineAccountsQML_BINARY_DIR}/libLomiriOnlineAccountsQML.so
                ${CMAKE_CURRENT_BINARY_DIR}/Lomiri/OnlineAccounts/libLomiriOnlineAccountsQML.so)

add_dependencies(${TEST} LomiriOnlineAccountsPluginQML)
add_custom_command(
        TARGET ${TEST} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                ${LomiriOnlineAccountsPluginQML_BINARY_DIR}/qmldir
                ${CMAKE_CURRENT_BINARY_DIR}/Lomiri/OnlineAccounts/Plugin/qmldir
        COMMAND ${CMAKE_COMMAND} -E copy
                ${LomiriOnlineAccountsPluginQML_BINARY_DIR}/libLomiriOnlineAccountsPluginQML.so
                ${CMAKE_CURRENT_BINARY_DIR}/Lomiri/OnlineAccounts/Plugin/libLomiriOnlineAccountsPluginQML.so)

set(QML_FILES_PLUGIN_MODULE
    ChromedWebView.qml
    DuplicateAccount.qml
    ErrorItem.qml
    KeyboardRectangle.qml
    OAuthMain.qml
    OAuth.qml
    Options.qml
    RemovalConfirmation.qml
    ServiceItem.qml
    ServiceItemBase.qml
    ServiceSwitches.qml
    StandardAnimation.qml
    WebView.qml
)
# copy qml files over to build dir to be able to import them uninstalled
foreach(_file ${QML_FILES_PLUGIN_MODULE})
    add_custom_command(TARGET ${TEST} POST_BUILD
                       COMMAND ${CMAKE_COMMAND} -E copy_if_different
                       ${LomiriOnlineAccountsPluginQML_SOURCE_DIR}/${_file}
                       ${CMAKE_CURRENT_BINARY_DIR}/Lomiri/OnlineAccounts/Plugin/${_file})
endforeach(_file)

add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
