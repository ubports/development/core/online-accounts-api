set(TEST tst_online-accounts-hooks)
set(SOURCES
    tst_online_accounts_hooks.cpp
)

pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)

add_executable(${TEST} ${SOURCES})
include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${ACCOUNTSQT_INCLUDE_DIRS}
)
add_definitions(
    -DHOOK_PROCESS="${lomiri-online-accounts-hooks_BINARY_DIR}/lomiri-online-accounts-hooks"
)
add_dependencies(${TEST} lomiri-online-accounts-hooks)

target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::Xml
    Qt${QT_VERSION}::Test
    ${ACCOUNTSQT_LIBRARIES}
)

set_target_properties(${TEST} PROPERTIES AUTOMOC TRUE)
add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
