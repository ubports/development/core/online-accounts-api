add_subdirectory(daemon)

set(TEST tst_daemon)
set(SOURCES
    daemon_interface.cpp
    functional_tests.cpp
)

pkg_check_modules(QTDBUSMOCK REQUIRED libqtdbusmock-1)
pkg_check_modules(QTDBUSTEST REQUIRED libqtdbustest-1)
pkg_check_modules(ACCOUNTSQT accounts-qt${QT_VERSION} REQUIRED)

add_executable(${TEST} ${SOURCES})
include_directories(
    ${CMAKE_CURRENT_BINARY_DIR}
    ${LomiriOnlineAccountsQt_SOURCE_DIR}/..
    ${LomiriOnlineAccountsDaemon_SOURCE_DIR}/..
    ${ACCOUNTSQT_INCLUDE_DIRS}
    ${QTDBUSMOCK_INCLUDE_DIRS}
    ${QTDBUSTEST_INCLUDE_DIRS}
)
add_definitions(
    -DTEST_DBUS_CONFIG_FILE="${CMAKE_CURRENT_BINARY_DIR}/data/testsession.conf"
    -DTEST_DATA_DIR="${CMAKE_CURRENT_SOURCE_DIR}/data"
    -DTEST_PROCESS="${CMAKE_CURRENT_SOURCE_DIR}/test_process.py"
    -DSIGNOND_MOCK_TEMPLATE="${CMAKE_CURRENT_SOURCE_DIR}/signond.py"
    -DDBUS_APPARMOR_MOCK_TEMPLATE="${CMAKE_CURRENT_SOURCE_DIR}/dbus_apparmor.py"
    -DONLINE_ACCOUNTS_SERVICE_MOCK_TEMPLATE="${CMAKE_CURRENT_SOURCE_DIR}/online_accounts-service.py"
)
add_dependencies(${TEST} loa-accountd)

configure_file(data/testsession.conf.in data/testsession.conf)
configure_file(data/com.lomiri.OnlineAccounts.Manager.service.in
    data/com.lomiri.OnlineAccounts.Manager.service
)
configure_file(loa-accountd-wrapper.in ${CMAKE_CURRENT_BINARY_DIR}/loa-accountd-wrapper)

target_link_libraries(${TEST}
    Qt${QT_VERSION}::Core
    Qt${QT_VERSION}::DBus
    Qt${QT_VERSION}::Test
    lomiri-online-accounts-qt${QT_VERSION}
    ${ACCOUNTSQT_LIBRARIES}
    ${QTDBUSMOCK_LIBRARIES}
    ${QTDBUSTEST_LIBRARIES}
)

set_target_properties(${TEST} PROPERTIES AUTOMOC TRUE)
add_test(${TEST} ${XVFB_COMMAND} dbus-test-runner -t ${CMAKE_CURRENT_BINARY_DIR}/${TEST})
